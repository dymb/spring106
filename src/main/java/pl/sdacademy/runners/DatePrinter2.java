package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.HumanReadableDateFormatter;
import pl.sdacademy.components.ShortDateFormatter;

import java.time.LocalDateTime;

@Component
public class DatePrinter2 implements CommandLineRunner {

    private DateFormatter formatter1;
    private DateFormatter formatter2;

    @Autowired
    public DatePrinter2(
            ShortDateFormatter formatter1,
            HumanReadableDateFormatter formatter2
    ) {
        this.formatter1 = formatter1;
        this.formatter2 = formatter2;
    }

    public void run(String... args) throws Exception {
        System.out.println("--------DatePrinter2----------");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Data: " + formatter1.format(dateTime));
        System.out.println("Data: " + formatter2.format(dateTime));
    }
}
