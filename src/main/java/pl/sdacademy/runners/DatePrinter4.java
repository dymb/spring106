package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;

import java.time.LocalDateTime;

@Component
public class DatePrinter4 implements CommandLineRunner {

    @Autowired
    @Qualifier("human")
    DateFormatter formatter1;

    @Autowired
    @Qualifier("short")
    DateFormatter formatter2;


    public void run(String... args) throws Exception {
        System.out.println("--------DatePrinter4----------");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Data: " + formatter1.format(dateTime));
        System.out.println("Data: " + formatter2.format(dateTime));
    }
}
