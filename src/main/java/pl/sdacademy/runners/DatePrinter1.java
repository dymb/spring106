package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.HumanReadableDateFormatter;
import pl.sdacademy.components.ShortDateFormatter;

import java.sql.Date;
import java.time.LocalDateTime;

@Component
public class DatePrinter1 implements CommandLineRunner {
    @Autowired
    HumanReadableDateFormatter formatter1;

    private DateFormatter formatter2;


    public void run(String... args) throws Exception {
        System.out.println("--------DatePrinter1----------");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Data: " + formatter1.format(dateTime));
        System.out.println("Data: " + formatter2.format(dateTime));
    }


    @Autowired
    public void setFormatter2(ShortDateFormatter formatter2) {
        this.formatter2 = formatter2;
    }
}
