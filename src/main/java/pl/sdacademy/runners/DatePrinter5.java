package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.ShortDateFormatter;

@Component
public class DatePrinter5 implements CommandLineRunner {

    private ApplicationContext context;

    @Autowired
    public DatePrinter5(ApplicationContext context) {
        this.context = context;
    }

    public void run(String... args) throws Exception {
        System.out.println("--------DatePrinter5----------");
        DateFormatter dateFormatter1 = context.getBean(ShortDateFormatter.class);
        DateFormatter dateFormatter2 = context.getBean(ShortDateFormatter.class);

        System.out.println("Is equals: " + (dateFormatter1 == dateFormatter2));
    }
}
