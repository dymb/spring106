package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.ShortDateFormatter;

import java.time.LocalDateTime;

@Component
public class DatePrinter implements CommandLineRunner {

    private DateFormatter formatter;

    public void run(String... args) throws Exception {
        LocalDateTime date = LocalDateTime.now();
        String formattedDate = formatter.format(date);
        System.out.println("Aktualna data: " + formattedDate);
    }

    @Autowired
    public void setFormatter(ShortDateFormatter formatter) {
        this.formatter = formatter;
    }
}
