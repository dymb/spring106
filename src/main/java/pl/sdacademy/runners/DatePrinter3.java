package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.HumanReadableDateFormatter;
import pl.sdacademy.components.ShortDateFormatter;

import java.time.LocalDateTime;

@Component
public class DatePrinter3 implements CommandLineRunner {

    @Autowired
    ApplicationContext context;

    public void run(String... args) throws Exception {
        DateFormatter formatter1 = context.getBean(HumanReadableDateFormatter.class);
        DateFormatter formatter2 = context.getBean(ShortDateFormatter.class);

        System.out.println("--------DatePrinter3----------");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("Data: " + formatter1.format(dateTime));
        System.out.println("Data: " + formatter2.format(dateTime));
    }
}
