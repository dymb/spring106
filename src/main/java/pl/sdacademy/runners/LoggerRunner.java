package pl.sdacademy.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sdacademy.components.AppLogger;
import pl.sdacademy.components.ConsoleLogger;
import pl.sdacademy.components.FileLogger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Component
public class LoggerRunner implements CommandLineRunner {

    private AppLogger logger;

    @Autowired
    public LoggerRunner(FileLogger logger) {
        this.logger = logger;
    }

    public void run(String... args) throws Exception {
        logger.logInfo("--------LoggerRunner-----------");
        logger.logError("Błąd krytyczny");
        logger.logDebug("Tutaj wstawiamy informacje testujące");
        logger.logInfo("Startuje metoda run...");
    }
}
