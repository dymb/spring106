package pl.sdacademy.controller;

import org.springframework.web.bind.annotation.*;
import pl.sdacademy.entity.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private ArrayList<User> users = new ArrayList<>(
            Arrays.asList(
                    new User(1, "Adrian", "Mazur", 20),
                    new User(2, "Wojciech", "Pokorny", 10),
                    new User(3, "Józef", "Grobelny", 55)
            )
    );

    @GetMapping("/users")
    public List<User> findAll(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "age", required = false) Integer age
    ) {
        List<User> foundUsers = users
                .stream()
                .filter(user -> name == null || user.getName().equalsIgnoreCase(name))
                .filter(user -> age == null || user.getAge().equals(age))
                .collect(Collectors.toList());

        return foundUsers;
    }

    @GetMapping("/userByName")
    public User findByName(
            @RequestParam("name") String name
    ) {
        User found = users
                .stream()
                .filter(user -> user.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);

        return found;
    }

    @GetMapping("/userByAge")
    public User findByAge(
            @RequestParam("age") Integer age
    ) {
        User found = users
                .stream()
                .filter(user -> user.getAge().equals(age))
                .findFirst()
                .orElse(null);

        return found;
    }

    @GetMapping("/users/{id}")
    public User getById(@PathVariable("id") Integer id) {
        User found = users
                .stream()
                .filter(user -> user.getId().equals(id))
                .findFirst()
                .orElse(null);

        return found;
    }

    @PutMapping(value = "/users", consumes = "application/json")
    public void insert(@RequestBody User user) {
        users.add(user);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable("id") Integer id) {
        User found = users
                .stream()
                .filter(user -> user.getId().equals(id))
                .findFirst()
                .orElse(null);
        if(found != null) {
            users.remove(found);
        }
    }

    @PostMapping(value = "/users", consumes = "application/json")
    public void update(@RequestBody User updateUser) {
        User found = users
                .stream()
                .filter(user -> user.getId().equals(updateUser.getId()))
                .findFirst()
                .orElse(null);

        if(found != null) {
            found.setAge(updateUser.getAge());
            found.setName(updateUser.getName());
            found.setLastName(updateUser.getLastName());
        }
    }
}
