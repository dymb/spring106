package pl.sdacademy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.components.AppLogger;
import pl.sdacademy.components.DateFormatter;
import pl.sdacademy.components.DetailsDateFormatter;
import pl.sdacademy.components.FileLogger;

import java.time.LocalDateTime;

@RestController
public class MainController {

    private DateFormatter dateFormatter;
    private AppLogger logger;

    private int counter = 0;

    @Autowired
    public MainController(
            DetailsDateFormatter dateFormatter,
            FileLogger logger
    ) {
        this.dateFormatter = dateFormatter;
        this.logger = logger;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        logger.logInfo("hello()");

        return "hej to ja Damian :)";
    }

    @RequestMapping(value = "/hello", method = RequestMethod.DELETE)
    public void helloDelete() {
    }


    @RequestMapping(value = "/showDate", method = RequestMethod.GET)
    public String showDate() {
        logger.logInfo("showDate()");

        LocalDateTime dateTime = LocalDateTime.now();
        return dateFormatter.format(dateTime);
    }


    @GetMapping("/add")
    public int add() {
        counter++;
        return counter;
    }

    @DeleteMapping("/sub")
    public void sub() {
        counter--;
    }

    @PostMapping("/reset")
    public void reset() {
        counter = 0;
    }

    @GetMapping("/addValues")
    public Integer addValues(
            @RequestParam("number1") Integer number1,
            @RequestParam("number2") Integer number2
    ) {
        Integer sum = number1 + number2;
        return sum;
    }

    @PostMapping("/welcome/{name}")
    public String welcome(
            @PathVariable("name") String name,
            @RequestParam("age") Integer age
    ) {
        return "Witaj " + name + " wygląda że masz " + age + " lat";
    }

    @PutMapping("/welcome/{name}")
    public String goodbye(
            @PathVariable("name") String name,
            @RequestParam("age") Integer age
    ) {
        return "Żegnaj " + name + " wygląda że miałeś " + age + " lat ale już po Tobie ;)";
    }
}
