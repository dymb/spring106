package pl.sdacademy.components;

import java.time.LocalDateTime;

public interface DateFormatter {
    String format(LocalDateTime dateTime);
}
