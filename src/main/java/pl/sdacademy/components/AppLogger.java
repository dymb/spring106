package pl.sdacademy.components;

public interface AppLogger {
    void logError(String message);
    void logDebug(String message);
    void logInfo(String message);
}
