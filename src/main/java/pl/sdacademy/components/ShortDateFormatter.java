package pl.sdacademy.components;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component("short")
@Scope("prototype")
public class ShortDateFormatter implements DateFormatter {

    @Value("${shortDateFormat}")
    private String pattern;

    public String format(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

        return dateTime.format(formatter);
    }

    @PostConstruct
    public void start() {
        System.out.println("Start ziarenka");
    }

    @PreDestroy
    public void stop() {
        System.out.println("Stop ziarenka");
    }
}
