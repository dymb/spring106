package pl.sdacademy.components;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

@Component
public class FileLogger implements AppLogger {

    @Value("${logPath}")
    private String logPath;
    private Path filePath;

    private DateFormatter formatter;

    public FileLogger(ShortDateFormatter formatter) {
        this.formatter = formatter;
    }

    public void logError(String message) {
        String text = "[ERROR] " + message + " " + formatDate();
        writeAndPrintText(text);
    }

    public void logDebug(String message) {
        String text = "[DEBUG] " + message + " " + formatDate();
        writeAndPrintText(text);
    }

    public void logInfo(String message) {
        String text = "[INFO] " + message + " " + formatDate();
        writeAndPrintText(text);
    }

    @PostConstruct
    public void start() {
        filePath = Paths.get(logPath);
    }

    private String formatDate() {
        LocalDateTime dateTime = LocalDateTime.now();
        return formatter.format(dateTime);
    }

    private void writeAndPrintText(String text) {
        try {
            System.out.println(text);

            Files.writeString(
                    filePath,
                    text + "\n",
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
