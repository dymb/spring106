package pl.sdacademy.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ConsoleLogger implements AppLogger {

    private DateFormatter formatter;

    @Autowired
    public ConsoleLogger(DetailsDateFormatter formatter) {
        this.formatter = formatter;
    }

    public void logError(String message) {
        System.err.println("[ERROR] " + message + " " + currentDate());
    }

    public void logDebug(String message) {
        System.out.println("[DEBUG] " + message + " " + currentDate());
    }

    public void logInfo(String message) {
        System.out.println("[INFO] " + message + " " + currentDate());
    }

    private String currentDate() {
        LocalDateTime dateTime = LocalDateTime.now();
        return formatter.format(dateTime);
    }
}
